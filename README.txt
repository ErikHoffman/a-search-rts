Erik Hoffman's A* Search algorithm 

To Test:
Open map.txt and change the X and O to desired starting position.
Open Astar.py and change destination at beginning of main function to desired end destination
Run and watch

Issues:
If the two pieces get stuck trying to get around each other they may break the tie or it may halt
If one piece completely blocks the other it will halt (add depth limit and then have move-out-of-the-way feature?)
