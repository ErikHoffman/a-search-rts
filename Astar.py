from __future__ import print_function;
from SearchProblem import *;
from random import randint;
import math;
import time;

"""def check_solvable( puzzle ):
	puzzle_l = list(puzzle)
	num_inversions = 0
	
	for x in range(0,len(puzzle_l)-1):
		for y in range(x+1,len(puzzle_l)):
			if puzzle_l[x] != 0 and puzzle_l[y] != 0 and puzzle_l[x] > puzzle_l[y]:
				num_inversions += 1

	if num_inversions%2 == 0:
		return True
	else:
		return False

def generate_puzzle( ):
	solvable = 0
	while solvable == 0:
		eight_puzzle = ""
		picked_numbers = []
		placing_unique = 0
		for x in range(0,9):

			placing_unique = 0
			while (placing_unique < 1):
				placing_unique = 0
				rand = randint(0,8)
				for y in range(0,len(picked_numbers)):
					if (rand == picked_numbers[y]):
						placing_unique = -1
				if placing_unique == 0:
					picked_numbers.append(rand)
					placing_unique = 1
		
			eight_puzzle += repr(rand)
		if check_solvable(eight_puzzle) == True:
			break
	return eight_puzzle

def move_up( puzzle, hole ):
	puzzle_l = list(puzzle)
	num = puzzle_l[hole+3]
	puzzle_l[hole+3] = '0'
	puzzle_l[hole] = str(num)
	puzzle = ''.join(puzzle_l)
	return puzzle

def move_down( puzzle, hole ):
	puzzle_l = list(puzzle)
	num = puzzle_l[hole-3]
	puzzle_l[hole-3] = '0'
	puzzle_l[hole] = str(num)
	puzzle = ''.join(puzzle_l)
	return puzzle

def move_left( puzzle, hole ):
	puzzle_l = list(puzzle)
	num = puzzle_l[hole+1]
	puzzle_l[hole+1] = '0'
	puzzle_l[hole] = str(num)
	puzzle = ''.join(puzzle_l)
	return puzzle

def move_right( puzzle, hole ):
	puzzle_l = list(puzzle)
	num = puzzle_l[hole-1]
	puzzle_l[hole-1] = '0'
	puzzle_l[hole] = str(num)
	puzzle = ''.join(puzzle_l)
	return puzzle

class Eight_Puzzle( SearchProblem ):
	def __init__( self, state, root=False ):
		self.state = state;
		self.path = "";
		if root:
			self.visited = [];

	def edges( self ):
		my_edges = [];

		if self.state[0] == 1:  # Top Left (2 moves, up and left)
		  	#move up
			new_puzzle = move_up(self.state[1],0)
			new_zero_location = 4
			my_edges.append( Edge( self, "u", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move left
			new_puzzle = move_left(self.state[1],0)
			new_zero_location = 2
			my_edges.append( Edge( self, "l", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )



		elif self.state[0] == 2:  # Top Mid (3 moves, up, right and left)
		  	#move up
			new_puzzle = move_up(self.state[1],1)
			new_zero_location = 5
			my_edges.append( Edge( self, "u", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move right
			new_puzzle = move_right(self.state[1],1)
			new_zero_location = 1
			my_edges.append( Edge( self, "r", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move left
			new_puzzle = move_left(self.state[1],1)
			new_zero_location = 3
			my_edges.append( Edge( self, "l", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )



		elif self.state[0] == 3:  # Top Right (2 moves, up and left)
		  	#move up
			new_puzzle = move_up(self.state[1],2)
			new_zero_location = 6
			my_edges.append( Edge( self, "u", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move right
			new_puzzle = move_right(self.state[1],2)
			new_zero_location = 2
			my_edges.append( Edge( self, "r", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )



		elif self.state[0] == 4:  # Mid Left (3 moves, up, down and left)
		  	#move up
			new_puzzle = move_up(self.state[1],3)
			new_zero_location = 7
			my_edges.append( Edge( self, "u", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

		  	#move down
			new_puzzle = move_down(self.state[1],3)
			new_zero_location = 1
			my_edges.append( Edge( self, "d", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move left
			new_puzzle = move_left(self.state[1],3)
			new_zero_location = 5
			my_edges.append( Edge( self, "l", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )



		elif self.state[0] == 5:  # Mid Mid (4 moves, up, down, left and right)
		  	#move up
			new_puzzle = move_up(self.state[1],4)
			new_zero_location = 8
			my_edges.append( Edge( self, "u", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

		  	#move down
			new_puzzle = move_down(self.state[1],4)
			new_zero_location = 2
			my_edges.append( Edge( self, "d", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move left
			new_puzzle = move_left(self.state[1],4)
			new_zero_location = 6
			my_edges.append( Edge( self, "l", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move right
			new_puzzle = move_right(self.state[1],4)
			new_zero_location = 4
			my_edges.append( Edge( self, "r", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )



		elif self.state[0] == 6:  # Mid Right (3 moves, up, down and left)
		  	#move up
			new_puzzle = move_up(self.state[1],5)
			new_zero_location = 9
			my_edges.append( Edge( self, "u", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

		  	#move down
			new_puzzle = move_down(self.state[1],5)
			new_zero_location = 3
			my_edges.append( Edge( self, "d", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move right
			new_puzzle = move_right(self.state[1],5)
			new_zero_location = 5
			my_edges.append( Edge( self, "r", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )



		elif self.state[0] == 7:  # Bot Left (2 moves, down and left)
		  	#move down
			new_puzzle = move_down(self.state[1],6)
			new_zero_location = 4
			my_edges.append( Edge( self, "d", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move left
			new_puzzle = move_left(self.state[1],6)
			new_zero_location = 8
			my_edges.append( Edge( self, "l", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )



		elif self.state[0] == 8:  # Bot Mid (3 moves, down, left and right)
		  	#move left
			new_puzzle = move_left(self.state[1],7)
			new_zero_location = 9
			my_edges.append( Edge( self, "l", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

		  	#move down
			new_puzzle = move_down(self.state[1],7)
			new_zero_location = 5
			my_edges.append( Edge( self, "d", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move right
			new_puzzle = move_right(self.state[1],7)
			new_zero_location = 7
			my_edges.append( Edge( self, "r", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

		elif self.state[0] == 9:  # Bot Right (2 moves, down and right)
			#move down
			new_puzzle = move_down(self.state[1],8)
			new_zero_location = 6
			my_edges.append( Edge( self, "d", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

			#move right
			new_puzzle = move_right(self.state[1],8)
			new_zero_location = 8
			my_edges.append( Edge( self, "r", Eight_Puzzle( ( new_zero_location, new_puzzle ) ) ) )

		#print my_edges;
		#time.sleep(2);
		return my_edges;

	def is_target( self ):
		return self.state[0]==9 and self.state[1] == "123456780";"""

def collision( map, unit, direction ):
	if direction == 0: #Up
		if map[unit.startY-1][unit.startX] == '.':
			return True
	elif direction == 1: #Down
		if map[unit.startY+1][unit.startX] == '.':
			return True
	elif direction == 2: #Left
		if map[unit.startY][unit.startX-1] == '.':
			return True
	elif direction == 3: #Right
		if map[unit.startY][unit.startX+1] == '.':
			return True
	return False

def puzzleReset( map ):
	new_puzzle = [[0 for x in range(20)] for x in range(20)]
	i = 0
	for line in map:
		new_puzzle[i] = list(map[i])
		i += 1
	return new_puzzle
	
class Astar( SearchProblem ):
	def __init__( self, map, unit, root=False ):
		self.map = map
		self.path = ""
		self.unit = unit
		if root:
			self.visited = [];


	def edges( self ):
		my_edges = [];
		#new_puzzle = [] #Changed for where the proposed unit move is
		new_unit = unit( self.unit.char, self.unit.startX, self.unit.startY, self.unit.destX, self.unit.destY)
		
		new_puzzleA = puzzleReset( self.map )
		#Check above
		if self.unit.startY > 0:	
			if (collision( self.map, self.unit, 0) == True): #Make sure we check collision inside if or it will out of bounds
				new_puzzleA[self.unit.startY][self.unit.startX] = '.'
				new_puzzleA[self.unit.startY-1][self.unit.startX] = self.unit.char
				new_unit.startY -= 1
				my_edges.append( Edge( self, self.unit.char + " up", Astar( new_puzzleA, new_unit ) ) )
		new_puzzleB = puzzleReset( self.map )
		#for line in new_puzzleB:
		#	print(line)
		#print("\n")
		#time.sleep(1)
		new_unit = unit( self.unit.char, self.unit.startX, self.unit.startY, self.unit.destX, self.unit.destY)
		#Check down
		if self.unit.startY < 19:
			if(collision( self.map, self.unit, 1) == True):
				new_puzzleB[self.unit.startY][self.unit.startX] = '.'
				new_puzzleB[self.unit.startY+1][self.unit.startX] = self.unit.char
				new_unit.startY += 1
				my_edges.append( Edge( self, self.unit.char + " down", Astar( new_puzzleB, new_unit ) ) ) 
		new_puzzleC = puzzleReset( self.map )
		new_unit = unit( self.unit.char, self.unit.startX, self.unit.startY, self.unit.destX, self.unit.destY)
		#Check left
		if self.unit.startX > 0:
			if(collision( self.map, self.unit, 2) == True):
				new_puzzleC[self.unit.startY][self.unit.startX] = '.'
				new_puzzleC[self.unit.startY][self.unit.startX-1] = self.unit.char
				new_unit.startX -= 1
				my_edges.append( Edge( self, self.unit.char + " left", Astar( new_puzzleC, new_unit ) ) )
		new_puzzleD = puzzleReset( self.map )
		new_unit = unit( self.unit.char, self.unit.startX, self.unit.startY, self.unit.destX, self.unit.destY)
		#Check right
		if self.unit.startX < 19:
			if(collision( self.map, self.unit, 3) == True):
				new_puzzleD[self.unit.startY][self.unit.startX] = '.'
				new_puzzleD[self.unit.startY][self.unit.startX+1] = self.unit.char
				new_unit.startX += 1
				my_edges.append( Edge( self, self.unit.char + " right", Astar( new_puzzleD, new_unit ) ) )

		return my_edges
	def is_target( self ):
		return self.map[self.unit.destY][self.unit.destX] == self.unit.char 

"""def create_puzzle(vehicles):

	###################################

	#create matrix
	puzzle = [[0 for x in range(20)] for x in range(20)]
	##################################################

	i = 0
	#loop through the vehicles we are placing
	for vehicle in vehicles:
		i = 0
		#loop, through the attributes of the vehicle
		for data in vehicle:
			
			if i == 0: #Our vehicle char
				vehicleChar = data
				#print(vehicleChar)
			elif i == 1: #X coordinate of the top 
				xCoord = int(data)
				#print(xCoord)
			elif i == 2:
				yCoord = int(data)
				#print(yCoord)
			elif i == 3:
				orientation = data
				#print(orientation)

			i = i + 1

		#After we have looked at a vehicle place it into the board
		#Do truck first since there are only 4 types of truck and that if statement would be outta control
		if orientation == 'V':
			if vehicleChar == 'O' or vehicleChar == 'P' or vehicleChar == 'Q' or vehicleChar == 'R':
				puzzle[yCoord][xCoord] = vehicleChar
				puzzle[yCoord+1][xCoord] = vehicleChar
				puzzle[yCoord+2][xCoord] = vehicleChar
			else:
				puzzle[yCoord][xCoord] = vehicleChar
				puzzle[yCoord+1][xCoord] = vehicleChar
		else:
			if vehicleChar == 'O' or vehicleChar == 'P' or vehicleChar == 'Q' or vehicleChar == 'R':
				puzzle[yCoord][xCoord] = vehicleChar
				puzzle[yCoord][xCoord+1] = vehicleChar
				puzzle[yCoord][xCoord+2] = vehicleChar
			else:
				puzzle[yCoord][xCoord] = vehicleChar
				puzzle[yCoord][xCoord+1] = vehicleChar

	return puzzle"""


class unit(object):
	def __init__(self, char, startX, startY, destX, destY):
		self.char = char
		self.startX = startX
		self.startY = startY
		self.destX = destX
		self.destY = destY

if __name__ == "__main__":

	board = []
	units = [] #This structure is gonna store our units, where they start and where to place them
	paths = []
	i = 0
	s = 0
	file = open('map.txt').readlines()

	map = [[0 for x in range(20)] for x in range(20)]
	i = j = 0
	for line in file:
		j = 0
		for char in line:
			if char != '\r' and char != '\n':
				map[i][j] = char
			j += 1
		i += 1
	print(map)
	#Define vehicle attribute variables
	playerChar = 'O'
	enemyChar = 'X'
	playerX = 17
	playerY = 4
	playerDestX = 8
	playerDestY = 18
	enemyX = 3
	enemyY = 17
	enemyDestX = 4
	enemyDestY = 4
	units.append(unit(playerChar,playerX,playerY,playerDestX,playerDestY))
	units.append(unit(enemyChar,enemyX,enemyY,enemyDestX,enemyDestY))

	#Define the start nodes for the two pieces
	
	#Take the list of the lines from the file and create our start state
	#Send the data to begin the search
	path = Astar( map=(map), unit=(units[0]), root=True).astarsearch()
	paths.append(path)
	path = Astar( map=(map), unit=(units[1]), root=True).astarsearch()
	paths.append(path)
	
	pathA = paths[0]
	pathB = paths[1]
	movesA = pathA.split( ">")
	movesA.pop() #Path has a > at the end of it so remove blank end
	
	movesB = pathB.split(">")
	movesB.pop()
	
	new_puzzle = puzzleReset(map)
	currPlayerX = playerX
	currPlayerY = playerY
	currEnemyX = enemyX
	currEnemyY = enemyY
	moveANum = moveBNum = 0 #The iterators for moves, reset when re-search
	while(1):
		new_puzzle = puzzleReset(new_puzzle)
		#Get the moves of the pieces
		if len(movesA) < len(movesB):
			moveB = movesB[moveBNum]
			moveBNum += 1
			if moveANum < len(movesA):
				moveA = movesA[moveANum]
				moveANum += 1
			else:
				moveA = "oom"
		elif len(movesB) < len(movesA):
			moveA = movesA[moveANum]
			moveANum += 1
			if moveBNum < len(movesB):
				moveB = movesB[moveBNum]
				moveBNum += 1
			else:
				moveB = "oom"
		else: #same number of moves in both
			moveA = movesA[moveANum]
			moveB = movesB[moveBNum]
			moveANum += 1
			moveBNum += 1
		if moveA != "oom":
			if "up" in moveA:
				if collision(new_puzzle, unit(playerChar,currPlayerX,currPlayerY,playerDestX,playerDestY),0) == True:
					new_puzzle[currPlayerY][currPlayerX] = "."
					new_puzzle[currPlayerY-1][currPlayerX] = playerChar
					currPlayerY -= 1
				else:
					path = Astar( map=(new_puzzle), unit=(unit(playerChar,currPlayerX,currPlayerY,playerDestX,playerDestY)), root=True).astarsearch()
					moveANum = 0
					movesA = path.split( ">")
					movesA.pop() 
			elif "down" in moveA:
				if collision(new_puzzle, unit(playerChar,currPlayerX,currPlayerY,playerDestX,playerDestY),1) == True:
					new_puzzle[currPlayerY][currPlayerX] = "."
					new_puzzle[currPlayerY+1][currPlayerX] = playerChar
					currPlayerY += 1
				else:
					path = Astar( map=(new_puzzle), unit=(unit(playerChar,currPlayerX,currPlayerY,playerDestX,playerDestY)), root=True).astarsearch()
					moveANum = 0
					movesA = path.split( ">")
					movesA.pop() 
			elif "left" in moveA:
				if collision(new_puzzle, unit(playerChar,currPlayerX,currPlayerY,playerDestX,playerDestY),2) == True:
					new_puzzle[currPlayerY][currPlayerX] = "."
					new_puzzle[currPlayerY][currPlayerX-1] = playerChar
					currPlayerX -= 1
				else:
					path = Astar( map=(new_puzzle), unit=(unit(playerChar,currPlayerX,currPlayerY,playerDestX,playerDestY)), root=True).astarsearch()
					moveANum = 0
					movesA = path.split( ">")
					movesA.pop() 
			elif "right" in moveA:
				if collision(new_puzzle, unit(playerChar,currPlayerX,currPlayerY,playerDestX,playerDestY),3) == True:
					new_puzzle[currPlayerY][currPlayerX] = "."
					new_puzzle[currPlayerY][currPlayerX+1] = playerChar
					currPlayerX += 1
				else:
					path = Astar( map=(new_puzzle), unit=(unit(playerChar,currPlayerX,currPlayerY,playerDestX,playerDestY)), root=True).astarsearch()
					moveANum = 0
					movesA = path.split(">")
					movesA.pop() 
		if moveB != "oom":
			if "up" in moveB:
				if collision(new_puzzle, unit(enemyChar,currEnemyX,currEnemyY,enemyDestX,enemyDestY),0) == True:
					new_puzzle[currEnemyY][currEnemyX] = "."
					new_puzzle[currEnemyY-1][currEnemyX] = enemyChar
					currEnemyY -= 1
				else:
					path = Astar( map=(new_puzzle), unit=(unit(enemyChar,currEnemyX,currEnemyY,enemyDestX,enemyDestY)), root=True).astarsearch()
					moveBNum = 0
					movesB = path.split( ">")
					movesB.pop() 
			elif "down" in moveB:
				if collision(new_puzzle, unit(enemyChar,currEnemyX,currEnemyY,enemyDestX,enemyDestY),1) == True:
					new_puzzle[currEnemyY][currEnemyX] = "."
					new_puzzle[currEnemyY+1][currEnemyX] = enemyChar
					currEnemyY += 1
				else:
					path = Astar( map=(new_puzzle), unit=(unit(enemyChar,currEnemyX,currEnemyY,enemyDestX,enemyDestY)), root=True).astarsearch()
					moveBNum = 0
					movesB = path.split(">")
					movesB.pop() 
			elif "left" in moveB:
				if collision(new_puzzle, unit(enemyChar,currEnemyX,currEnemyY,enemyDestX,enemyDestY),2) == True:
					new_puzzle[currEnemyY][currEnemyX] = "."
					new_puzzle[currEnemyY][currEnemyX-1] = enemyChar
					currEnemyX -= 1
				else:
					path = Astar( map=(new_puzzle), unit=(unit(enemyChar,currEnemyX,currEnemyY,enemyDestX,enemyDestY)), root=True).astarsearch()
					moveBNum = 0
					movesB = path.split( ">")
					movesB.pop() 
			elif "right" in moveB:
				if collision(new_puzzle, unit(enemyChar,currEnemyX,currEnemyY,enemyDestX,enemyDestY),3) == True:
					new_puzzle[currEnemyY][currEnemyX] = "."
					new_puzzle[currEnemyY][currEnemyX+1] = enemyChar
					currEnemyX += 1
				else:
					path = Astar( map=(new_puzzle), unit=(unit(enemyChar,currEnemyX,currEnemyY,enemyDestX,enemyDestY)), root=True).astarsearch()
					moveBNum = 0
					movesB = path.split(">")
					movesB.pop()
		#new_puzzle = puzzleReset(new_puzzle)
		for lines in new_puzzle:
			print(lines)
		time.sleep(0.5)
		if new_puzzle[playerDestY][playerDestX] == playerChar and new_puzzle[enemyDestY][enemyDestX] == enemyChar:
			print("Complete!\n")
			break
		else:
			print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
			
		
	#for lines in map:
	#	print(lines)
	#for move in moves:
	#	details = move.split(" ")
	#	if(details[1] == "up"):
	#		print("up")
	#	elif(details[1] == "down"):
	#		print("down")
	#	elif(details[1] == "left"):
	#		print("left")
	#	elif(details[1] == "right"):
	#		print("right")
	#Now we have the paths, let's move them at the same time
