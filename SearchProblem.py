import Queue
import sys
import time


def H1( state, depth ):

  #print(state)
  #time.sleep(5)
  i = 0
  j = 0
  k = 0
  heuristic = 0
  for chars in state[2]: #Loop through space on the 
    if chars == 'X':
      break
    i += 1

  i -= 1
  vehicleBlock = '' #This is the vehicle that is blocking our path
  #i now holds our x position
  for j in range(i+2,6):
    vehicleBlock = state[2][j]
    heuristicUp = 0
    heuristicDown = 0
    if vehicleBlock == 'O' or vehicleBlock == 'P' or vehicleBlock == 'Q' or vehicleBlock =='R': #truck
      #Loop down from the truck (since a truck cannot move up out of the way in this case) and for everything that isn't the truck itself add +1
      if state[3][j] != vehicleBlock and state[3][j] != 0: 
        heuristic += 1
      if state[4][j] != vehicleBlock and state[4][j] != 0:
        heuristic += 1
      if state[5][j] != vehicleBlock and state[4][j] != 0:
        heuristic += 1
      #See how far the truck is up
      if state[1][j] == vehicleBlock:
        heuristic += 1 #We need to move the truck this amount (account for in front of x as well)
      if state[0][j] == vehicleBlock:
        heuristic += 1 #We would have a total of 3 now
      heuristic += 1 #Truck will have to move at least one if it is blocking
    else: #Car in the way
      #Check up blocking cars
      if state[1][j] != vehicleBlock and state[1][j] != 0:
        heuristicUp += 1
        heuristicUp += 1 #The car will have to move twice to get out of the way
      if state[0][j] != vehicleBlock and state[0][j] != 0:
        heuristicUp += 1
        if state[0][j] == state[1][j]:
          heuristicUp += 1000 #This means another vertical car is blocking this car CANNOT move this way
        else:
          heuristicUp += 1

      #Now check down blocking cars
      if state[3][j] != vehicleBlock and state[3][j] != 0:
        heuristicDown += 1
        heuristicDown += 1
      if state[4][j] != vehicleBlock and state[4][j] != 0:
        if state[4][j] == state[3][j]:
          heuristicDown += 1000 
        else:
          heuristicDown += 1
      if state[5][j] != vehicleBlock and state[5][j] != 0:
        if state[5][j] == state[4][j]:
          heuristicDown += 1000
        else:
          heuristicDown += 1

      if state[1][j] == vehicleBlock:
        heuristicUp += 1
        heuristicDown += 2
      if state[3][j] == vehicleBlock:
        heuristicUp += 2
        heuristicDown += 1
      #Now compare moves
      if heuristicUp > heuristicDown:
        heuristic += heuristicDown
      else: #If they are equal it'll go here as well. Doesn't matter
        heuristic += heuristicUp 
      
    heuristic += 1 #Add 1 for each space the red car needs to move
  #print(heuristic)
  #print(heuristic+depth)
  #print(depth)
  return heuristic+depth

def H2( state, depth ):
  heuristic = 0
  i = 0
  for chars in state[2]: #Loop through space on the 
    if chars == 'X':
      break
    i += 1

  if i == 1:
    heuristic = 4
  elif i == 2:
    heuristic = 3
  elif i == 3: 
    heuristic = 2
  else:
    heuristic = 1
  
  return heuristic + depth

def astarH( state, depth ):
  heuristic = 0
  heur_horiz = 0
  heur_vert = 0
  if state.unit.startX > state.unit.destX:
	heur_horiz = state.unit.startX - state.unit.destX
  else:
	heur_horiz = state.unit.destX - state.unit.startX

  if state.unit.startY > state.unit.destY:
    heur_vert = state.unit.startY - state.unit.destY
  else:
    heur_vert = state.unit.destY - state.unit.startY

  heuristic =  heur_horiz + heur_vert
  return heuristic + depth
  
  




class SearchProblem:
  """
  This class represents the superclass for a search problem.

  Programmers should subclass this superclass filling in specific
  versions of the methods that are stubbed below.
  """

  stop = False;	# class variable to end search - single variable accessible to
                # all instances of the class

  visited = [];	# class variable that holds the states visited along the
		# path to the current node - used to avoid loops

  states_visited = 0 


  def __init__( self, state=None ):
    """
    Stub
    Constructor function for a search problem.

    Each subclass should supply a constructor method that can operate with
    no arguments other than the implicit "self" argument to create the
    start state of a problem.

    It should also supply a constructor method that accepts a "state"
    argument containing a string that represents an arbitrary state in
    the given search problem.

    It should also initialize the "path" member variable to a (blank) string.
    """
    raise NotImplementedError("__init__");

  def edges( self ):
    """
    Stub
    This method must supply a list or iterator for the Edges leading out 
    of the current state.
    """
    raise NotImplementedError("edges");

  def is_target( self ):
    """
    Stub
    This method must return True if the current state is a goal state and
    False otherwise.
    """

    raise NotImplementedError("is_target");

  def __repr__( self ):
    """
    This method must return a string representation of the current state
    which can be "eval"ed to generate an instance of the current state.
    """

    return self.__class__.__name__ + "( " + repr(self.map) + ")";

  def target_found( self ):
    """
    This method is called when the target is found.

    By default it prints out the path that was followed to get to the 
    current state.
    """
    print( "Solution: " + self.path );
    depth = self.path.count('>')
    print depth
    print "States Visited: " + str(SearchProblem.states_visited)
		

  def continue_search( self ):
    """
    This method should return True if the search algorithm is to continue
    to search for more solutions after it has found one, or False if it
    should not.
    """
    return False;

  def dfs( self ):
    SearchProblem.states_visited += 1
    """
    Perform a depth first search originating from the node, "self".
    Recursive method.
    """

    # print statement for debugging
    print( self.path ); 

    if SearchProblem.stop:	# check class variable and stop searching...
      return;

    if len(self.path) < 50: # Make sure we're not going to an insane depth
      for action in self.edges(): # consider each edge leading out of this node
        action.destination.path = self.path + str(action.label) + ">";	
					# get the label associated with the
					# action and append it to the path
					# string

        if repr(action.destination.state) in SearchProblem.visited:
          continue;		# skip if we've visited this one before

        SearchProblem.visited.append( repr(self.state) );

        if action.destination.is_target(): # check if destination of edge is target node
          action.destination.target_found();	# perform target found action
          if not self.continue_search():  # stop searching if not required
            SearchProblem.stop = True;    # set class variable to record that we
            break;                        # are done

        try:
          action.destination.dfs();			# resume recursive search 
        except RuntimeError as re:
          continue

        SearchProblem.visited.pop();

  def bfs( self, depth ):
    sizeOfDepth = 0
    q = Queue.Queue()
    p = Queue.Queue()
    for begin in self.edges():
      #print(begin)
      #begin.destination.path = begin.destination.path + str(begin.label) + ">"
      q.put(begin)
      sizeOfDepth += 1
      SearchProblem.visited.append( repr(begin.source.state))
      SearchProblem.states_visited += 1
    print "Depth 1 Size: " + str(sizeOfDepth)
    for x in range(1,depth): #We already looked at depth 1 so start at 1
      sizeOfDepth = 0
      p.queue.clear()
      while not q.empty():
        action = q.get() #grab an edge from the queue
        action.destination.path = action.source.path + str(action.label) + ">"
        for sub_act in action.destination.edges(): # consider each edge leading from action
          SearchProblem.states_visited += 1
          sub_act.destination.path = action.destination.path + str(sub_act.label) + ">";
          if repr(sub_act.destination.state) in SearchProblem.visited: #skip if we've seen already
            continue		# skip if we've visited this one before
          #if x == 1:
            #print(sub_act)
          #if x == 21:
            #print(sub_act)
          #print(sub_act)
          sizeOfDepth += 1
          #print sub_act
          SearchProblem.visited.append( repr(sub_act.destination.state) );
          if sub_act.destination.is_target(): #check if that edge is solution
            sub_act.destination.target_found()
            if not self.continue_search():
              SearchProblem.stop = True
              return

          p.put(sub_act) #place it at the back of the queue
      while not p.empty():
        q.put(p.get())
      print "Depth " + str(x+1) + " Size: " + str(sizeOfDepth)

  def bestfs( self ):
    found = 0
    q = Queue.PriorityQueue()
    for begin in self.edges():
      heuristic = H2(begin.destination.state,0)
      #print("YO")
      #print(begin)
      SearchProblem.visited.append( repr(begin.destination.state))
      SearchProblem.states_visited += 1
      q.put((heuristic,begin,0))
    #while not q.empty():
      #action = q.get()
      #print(action[0])
    #time.sleep(5)
    print(q.qsize())
    while not q.empty():
      action = q.get()
      #print(action[1])
      #time.sleep(5)
      action[1].destination.path = action[1].source.path + str(action[1].label) + ">"
      for sub_act in action[1].destination.edges():
        SearchProblem.states_visited += 1
        sub_act.destination.path = action[1].destination.path + str(sub_act.label) + ">"
        if repr(sub_act.destination.state) in SearchProblem.visited: #skip if we've seen already
          continue		# skip if we've visited this one before 
        SearchProblem.visited.append( repr(sub_act.destination.state) );
        if sub_act.destination.is_target(): #check if that edge is solution
          sub_act.destination.target_found()
          if not self.continue_search():
            SearchProblem.stop = True
            return
      
        heuristic = H2(sub_act.destination.state,action[2]+1)
        q.put((heuristic,sub_act,action[2]+1))
		
  def astarsearch( self ):
    found = 0
    q = Queue.PriorityQueue()
    #SearchProblem.visited.append( repr(self.map))
    for begin in self.edges():
      heuristic = astarH(begin.destination,0)
      SearchProblem.visited.append( repr(begin.destination.map))
      SearchProblem.states_visited += 1
      q.put((heuristic,begin,0))
    #while not q.empty():
     # action = q.get()
      #print(action[0])
      #time.sleep(5)
    #print(q.qsize())
    while not q.empty():
      action = q.get()
      #print(action[1])
      #time.sleep(5)
      action[1].destination.path = action[1].source.path + str(action[1].label) + ">"
      for sub_act in action[1].destination.edges():
        SearchProblem.states_visited += 1
        sub_act.destination.path = action[1].destination.path + str(sub_act.label) + ">"
        if repr(sub_act.destination.map) in SearchProblem.visited: #skip if we've seen already
          continue		# skip if we've visited this one before 
        SearchProblem.visited.append( repr(sub_act.destination.map) );
        if sub_act.destination.is_target(): #check if that edge is solution
          sub_act.destination.target_found()
          if not self.continue_search():
            SearchProblem.stop = True
            return sub_act.destination.path
      
        heuristic = astarH(sub_act.destination,action[2]+1)
        q.put((heuristic,sub_act,action[2]+1))
  

class Edge:
  """
  This class represents an edge between two nodes in a SearchProblem.
  Each edge has a "source" (which is a subclass of SearchProblem), a
  "destination" (also a subclass of SearchProblem) and a text "label".
  """

  def __init__( self, source, label, destination ):
    """
    Constructor function assigns member variables "source", "label" and
    "destination" as specified.
    """
    self.source = source;
    self.label = label;
    self.destination = destination;

  def __repr__( self ):
    return "Edge(" + repr( self.source ) + "," + \
                     repr( self.label ) + "," + \
                     repr( self.destination ) + ")";
